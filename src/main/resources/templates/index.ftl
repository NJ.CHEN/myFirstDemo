<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>留言板</title>
    <style>

    </style>
    <!---引入外部框架-->
    <link rel="stylesheet" type="text/css" href="../assets/css/linkcss/bootstrap.css">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <#--实现Vue异步-->
    <script src="https://cdn.bootcss.com/vue-resource/1.5.1/vue-resource.min.js"></script>

</head>
<body>
<!--主体-->
<div class="container" id="app">

    <div class="input-group ">
        <input type="text" class="form-control" v-model="searchInfo.name"  placeholder="Search by name">
        <span class="input-group-btn">
            <button class="btn btn-default" v-on:click="queryOneByName" data-toggle="modal" data-target="#querys" >Go!</button>
        </span>
    </div>
    <!--展示模板-->
    <table class="table table-hover table-bordered">
        <tr class="">
            <td colspan="5">
                <input type="button" value="添加" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#insert" >
            </td>
        </tr>
        <!--表格的标题-->
        <caption class="h3 text-center">用户信息表</caption>
        <tr class="">
            <td class="">序号</td>
            <td class="">姓名</td>
            <td class="">手机号</td>
            <td class="">生日</td>
            <td class="">操作</td>
        </tr>
        <!--循环展示vue实例属性里的值 -->
        <tr class="" v-for="(item,index) in myData">
            <td class="">{{index+1}}</td>
            <td class="">{{item.name}}</td>
            <td class="">{{item.tel}}</td>
            <td class="">{{item.birthday}}</td>
            <td >
                <input type="button" value="删除" class="btn btn-primary btn-sm" data-toggle="modal"
                       data-target="#delete" v-on:click="currentIndex=item.id" >
                <input type="button" value="编辑" class="btn btn-primary btn-sm" data-toggle="modal"
                       data-target="#edit" v-on:click="edit_before(item)" >
            </td>
        </tr>
        <!--单列一行展示删除全部-->
        <tr class="" v-if="myData.length!=0">
            <td colspan="5">
                <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#delete" v-on:click="">删除全部</button>
            </td>
        </tr>
        <!--单列一行提示暂无数据-->
        <tr class="" v-if="myData.length==0">
            <td colspan="4">
                <p class="text-center">暂无数据</p>
            </td>
        </tr>
    </table>


    <#--模态框-->

    <#--添加模块-->
    <div class="modal fade" id="insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加用户</h4>
                </div>
                <div class="modal-body" @submit.prevent="add">
                    <!--添加元素的form表单-->
                    <form>
                        <div class="form-group">
                            <label for="name">姓名：</label>
                            <input type="text" class="form-control" id="name" v-model="searchInfo.name" placeholder="请输入姓名">
                        </div>
                        <div class="form-group">
                            <label for="tel">手机号：</label>
                            <input type="text" class="form-control" id="tel" v-model="searchInfo.tel" placeholder="请输入手机号">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日：</label>
                            <input type="text" class="form-control" id="birthday" v-model="searchInfo.birthday" placeholder="请输入生日">
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" v-on:click="add()">确认</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <#--删除模块-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">您确认删除吗？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary" v-on:click="deleteOne">确认</button>
                </div>
            </div>
        </div>
    </div>

    <#--编辑模块-->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑用户</h4>
                </div>
                <div class="modal-body">
                    <!--添加元素的form表单-->
                    <form>
                        <div class="form-group">
                            <label for="name">姓名：</label>
                            <input type="text" class="form-control" id="name" v-model="searchInfo.name" placeholder="请输入姓名">
                        </div>
                        <div class="form-group">
                            <label for="tel">手机号：</label>
                            <input type="text" class="form-control" id="tel" v-model="searchInfo.tel" placeholder="请输入手机号">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日：</label>
                            <input type="text" class="form-control" id="birthday" v-model="searchInfo.birthday" placeholder="请输入生日">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" v-on:click="reset">重置</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary" v-on:click="confirm_edit">确认</button>
                </div>
            </div>
        </div>
    </div>

    <#--查询展示模块-->

    <div role="dialog" class="modal"  id="querys" >
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <table class="table table-bordered table-hover table-responsive" >
                        <caption  class="h3 text-info text-center ">用户信息表</caption>
                        <tr class="text-info">
                            <th class="text-center">序号</th>
                            <th class="text-center">用户名</th>
                            <th class="text-center">年龄</th>
                            <th class="text-center">班级</th>
                            <th class="text-center">操作</th>
                        </tr>
                        <tr class="text-center" v-for="(item,index) in queryData">
                            <td >{{index+1}}</td>
                            <td >{{item.name}}</td>
                            <td >{{item.tel}}</td>
                            <td >{{item.birthday}}</td>
                            <td >
                                <input type="button" value="删除" class="btn btn-primary btn-sm" data-toggle="modal"
                                       data-target="#delete" v-on:click="currentIndex=item.id" >
                                <input type="button" value="编辑" class="btn btn-primary btn-sm" data-toggle="modal"
                                       data-target="#edit" v-on:click="edit_before(item)" >
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<!-- Script相关代码-->
<script>
    var vm = new Vue({
        el:"#app",
        data:{
            myData:[],//所有数据
            searchInfo: {
                id:null,
                name: "",
                tel: "",
                birthday: ""
            },
            currentIndex:'',
            queryData:[]//按条件查找数据
        },
        methods:{
            /*添加一个信息*/
            add:function () {
                alert(1);
                console.log(this.searchInfo);
                //发送post请求
                this.$http.post("/user/add", this.searchInfo)
                        .then(function () {
                            alert("添加成功！");
                            this.queryAll();
                        }, function (error) {
                            alert(error.body.msg);
                        });
            },
            /*删除一个信息*/
            deleteOne:function(){
                this.$http.post("/user/deleteOne?currentIndex="+this.currentIndex)
                        .then(function (response) {
                            alert("删除成功");
                            this.queryAll();
                        },function (error) {
                            alert(error.body.msg);
                        })
            },
            /*更新输入框*/
            reset:function () {
                this.searchInfo.id = null;
                this.searchInfo.name="";
                this.searchInfo.birthday="";
                this.searchInfo.tel="";
            },
            /*查询所有数据*/
            queryAll:function () {
                this.$http.get("/user/queryAll",this.searchInfo)
                        .then(function (response) {
                            this.myData = response.data.userList;
                        },function (error) {
                           alert(error.body.msg)
                        });
            },
            /*编辑一条数据之前*/
            edit_before:function (item) {
                this.searchInfo.id = item.id;
                this.searchInfo.name = item.name;
                this.searchInfo.tel = item.tel;
                this.searchInfo.birthday = item.birthday;
            },
            /*编辑一条数据到模态框确认时*/
            confirm_edit:function () {
                console.log(this.searchInfo);
                this.$http.post("/user/edit",this.searchInfo)
                        .then(function (response) {
                            alert("编辑成功！")
                        },function () {
                            alert("请求处理失败")
                        })
            },
            /*按名字查询一条信息*/
            queryOneByName:function () {
                console.log(this.searchInfo.name);
                alert(1);
                this.$http.get("/user/queryOneByName?name="+this.searchInfo.name)
                        .then(function (response) {
                            console.log(response);
                            this.queryData = response.data.userList;
                        },function () {
                            alert("请求失败")
                        })
            }
        },
        created:function(){
            this.queryAll();
        }
    })
</script>
</body>
</html>