package com.henu.demo.indexcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class IndexController {

    @RequestMapping("/index")
    public String index(){
        System.out.println("到达后端页面");
        return "index";
    }
}
