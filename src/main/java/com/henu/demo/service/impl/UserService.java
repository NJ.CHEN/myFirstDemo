package com.henu.demo.service.impl;

import com.henu.demo.dao.UserDao;
import com.henu.demo.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements com.henu.demo.service.UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void insert(User user) {
        userDao.insert(user);
    }

    @Override
    public List<User> queryAll() {
        return userDao.queryAll();
    }

    @Override
    public void deleteOne(Integer currentIndex) {
        userDao.deleteOne(currentIndex);
    }

    @Override
    public void edit(User user) {
        userDao.edit(user);
    }

    @Override
    public List<User> queryOneByName(String name) {
        return userDao.queryOneByName(name);
    }
}
