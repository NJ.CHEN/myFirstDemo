package com.henu.demo.service;


import com.henu.demo.po.User;

import java.util.List;

public interface UserService {

    //添加一个用户
    void insert(User user);

    //查询所有用户
    List<User> queryAll();

    //删除一个用户
    void deleteOne(Integer currentIndex);

    void edit(User user);

    List<User> queryOneByName(String name);
}
