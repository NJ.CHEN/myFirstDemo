package com.henu.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.henu.demo.dao")
public class Demo123Application {
    public static void main(String[] args) {
        SpringApplication.run(Demo123Application.class, args);
    }
}
