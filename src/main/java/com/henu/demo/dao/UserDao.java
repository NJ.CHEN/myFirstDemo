package com.henu.demo.dao;


import com.henu.demo.po.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {

    //添加一个信息
    void insert(User user);

    //查询全部信息
    List<User> queryAll();

    //删除一条数据
    void deleteOne(Integer currentIndex);

    //编辑一条数据
    void edit(User user);

    List<User> queryOneByName(String name);
}
