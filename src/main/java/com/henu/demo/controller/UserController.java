package com.henu.demo.controller;


import com.henu.demo.po.User;
import com.henu.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    //添加
    @PostMapping("/add")
    public void insert(@RequestBody User user){
        System.out.println("后端的add方法");
        System.out.println(user);
        userService.insert(user);
    }
    //查询所有数据
    @GetMapping("/queryAll")
    public HashMap<String,List<User>> queryAll(){
        List<User> userList = userService.queryAll();
        System.out.println(userList.size());
        //打印输出
//        for(User u:userList){
//            System.out.println(u.toString());
//        }
        HashMap<String,List<User>> userMap = new HashMap<String, List<User>>();
        userMap.put("userList",userList);
        return userMap;
    }
    //删除一条数据
    @PostMapping("/deleteOne")
    public void deleteOne(@RequestParam("currentIndex") Integer currentIndex){
        System.out.println("需要删除的索引："+currentIndex);
        userService.deleteOne(currentIndex);
    }
    //编辑一条数据
    @PostMapping("/edit")
    public void edit(@RequestBody User user){
        System.out.println("编辑一条数据"+user.toString());
        userService.edit(user);
    }
    //按名字查询
    @GetMapping("/queryOneByName")
    public HashMap<String,List<User>> queryOneByName(@RequestParam("name") String name){
        List<User> userList = userService.queryOneByName(name);
        System.out.println(userList.size());
        //打印输出
        for(User u:userList){
            System.out.println(u.toString());
        }
        HashMap<String,List<User>> userMap = new HashMap<String, List<User>>();
        userMap.put("userList",userList);
        return userMap;
    }

}
